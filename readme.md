## Its simple crud application by nextjs + strapi cms + postgres in docker containers

For starting application you have to next steps:

```
cd .\dev-config\
```

```
docker-compose build
docker-compose up
```

After that steps apllication will started on localhost: frontend - 3000 port, cms - 1337 port and db on 5342 port. But if u wonna check app ability you should sign up in strapi on the 1337 port and create api_token in the Settings -> Api token. After that past this token in the env file to filed "API_TOKEN". Restart docker-compose.
Now you have working app and you can create instance of post in the cms: Content Manager -> choice entry "New" -> Create new entry -> Save and publish.
After that this instance will save in db and you can see your blog card on the fronted, just restart the browser page.
